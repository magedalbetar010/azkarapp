

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushNamed(context, '/main_screen');
    });
  }
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: Stack(
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text('مسبحتي',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    fontFamily: 'Tajawal',



                  ),),
                Text('اذكار اسلامية',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    // fontWeight: FontWeight.bold,
                    fontSize: 18,
                    fontFamily: 'Tajawal',
                  ),),

              ],
            ),
          ),
          Positioned(child: Text('مسبحتي',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 25,
                fontFamily: 'Tajawal',
              )

          ),
          left: 0,right: 0,bottom: 10,


          )
          // Align(
          //   alignment: Alignment.bottomCenter,
          //   child: Container(
          //     margin: EdgeInsetsDirectional.only(bottom: 10),
          //     child: Text('مسبحتي',
          //         style: TextStyle(
          //           color: Colors.white,
          //           // fontWeight: FontWeight.bold,
          //           fontSize: 25,
          //           fontFamily: 'Tajawal',
          //         )
          //
          //     ),
          //   ),
          // )
        ],
      ),
    );
  }
}
