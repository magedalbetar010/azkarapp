
import 'package:flutter/material.dart';
import 'dart:core';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int continar = 0;
  int container2=0;
  String _getMajed(){
    if(
    continar>=0&&continar<=33
    ){
      return ('سبحان الله');
    }else if(continar>33&&continar<=66){
      return('الحمدلله');
    }
    else if(continar>66&&continar<=99){
      return('الله اكبر');
    }

    else{
      return' لا إله إلا اللهُ وحده لا شريك له، له الملكُ وله الحمد';
    }
  }
  void rest(){
    if(continar>100){
      continar=0;
      _getMajed();
    }
  }
  void nextNumber(){
    if(continar==100){
      container2= container2+100;

    }
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.teal,
            title: Text('مسبحتي',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: 'Tajawal',
              fontWeight: FontWeight.bold,
            ),
            ),
            
          ),
          body: Container(
            padding: EdgeInsets.all(30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/image_2.jpg'),
                fit: BoxFit.cover,
                

              ),

            ),
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.teal[700],
                    image: DecorationImage(
                      image: AssetImage('images/image_1.jpg'),
                      fit: BoxFit.cover,
                    )
                  ),
                ),
                Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      width: 2,
                      color: Colors.teal.shade700,
                    )
                  ),
                  // margin: EdgeInsetsDirectional.only(start: 30,end: 30,top: 30),
                  child: Container(

                      height: 60,

                      alignment: Alignment.center,
                      width: double.infinity,
                      child: Row(
                        children: [
                          Expanded(

                              child: Center(
                                child: Text(_getMajed(),style: TextStyle(
                                  color: Colors.black,
                                    fontFamily: 'Tajawal',
                                  fontSize: 20,

                                ),),
                              ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: double.infinity,
                            width: 50,
                            decoration: BoxDecoration(
                              // color: Color.fromARGB(100, 178, 223, 219),
                              color: Colors.teal[200],
                              // borderRadius: BorderRadius.only(topLeft:Radius.circular(10),bottomLeft: Radius.circular(10)),

                              ),
                            child: Text(continar.toString(),style: TextStyle(
                              color: Colors.white,
                              fontSize: 23,
                              fontWeight: FontWeight.bold,
                                fontFamily: 'Tajawal'


                            ),),

                            ),
                          Container(
                            alignment: Alignment.center,
                            height: double.infinity,
                            width: 50,
                            decoration: BoxDecoration(
                              color: Colors.teal,
                              borderRadius: BorderRadius.only(topLeft:Radius.circular(10),bottomLeft: Radius.circular(10)),

                            ),
                            child: Text(container2.toString(),style: TextStyle(
                                color: Colors.white,
                                fontSize: 23,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Tajawal'


                            ),),

                          ),

                        ],
                      ),),
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10),
                              topRight: Radius.circular(10),
                            )
                          ),
                          color: Colors.teal,
                          onPressed: (){

                            setState(() {
                              continar++;
                              nextNumber();
                              rest();
                            });
                          },child: Text('إضافة',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 23,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Tajawal'
                          )
                          ,),

                        )),
                    MaterialButton(onPressed: (){
                      setState(() {
                        continar=0;
                        container2=0;
                      });
                    },
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topLeft: Radius.circular(10),
                          )
                      ),
                      child: Text('اعادة',style: TextStyle(
                        color: Colors.teal,
                          fontSize: 23,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Tajawal'
                      ),),)

                  ],
                )

              ],
            ),
          ),
        ),
      ),
    );
  }

}

